package com.cda.exec;

import java.util.Scanner;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.afpa.api.ITools;

public class Program {

	public static void main(String[] args) {
		String conf = "config.xml";
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(conf);
		
		ITools bean = ctx.getBean(ITools.class);
		
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("entrer un mot : ");
			System.out.print(">");
			String unMot = sc.nextLine();
			System.out.println(bean.action(unMot));
		}
	}

}
