package myapp.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import myapp.services.ICalculator;
import myapp.services.ILogger;

public class TestSpringJunitIntegration {

	@Autowired
	ILogger logger;

	@Autowired
	ICalculator calc;

	@BeforeEach
	public void beforeEachTest() {
		System.err.println("====================");
	}

	void use(ILogger logger) {
		logger.log("Voila le r�sultat");
	}

	void use(ICalculator calc) {
		calc.add(100, 200);
	}

	@Test
	public void testStderrLogger() {
		System.err.println("+++ StderrLogger");
		use(logger);
	}

	@Test
	public void testCalculatorWithLogger() {
		use(calc);
	}
}
