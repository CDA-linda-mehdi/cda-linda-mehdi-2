package myapp.tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import myapp.imp.BeanFileLogger;
import myapp.imp.FileLogger;
import myapp.imp.SimpleCalculator;
import myapp.imp.StderrLogger;
import myapp.services.ICalculator;
import myapp.services.ILogger;

@TestMethodOrder(OrderAnnotation.class)
public class TestLoggerServices {

	@BeforeEach
	public void beforeEachTest() {
		System.err.println("=============================");
	}
	
	@AfterEach
	public void afterEachTest() {
		
	}
	
	// use a logger
	void use(ILogger logger) {
		logger.log("Voil� le r�sultat = Hello");
	}
	
	// use a other logger
		void use(ICalculator calc) {
			calc.add(100, 200);
		}
	
	// Test StderrLogger
	@Order(1)
	@Test
	public void testStderrLogger() {
		// create the service
		StderrLogger logger = new StderrLogger();
		// start the service
		logger.start();
		// use the service
		use(logger);
		// stop the service
		logger.stop();
	}
	
	@Order(2)
	@Test
	public void testFileLogger() {
		FileLogger logger = new FileLogger("tmp/myapp.log");
		logger.start();
		use(logger);
		logger.stop();
	}
	
	@Order(3)
	@Test
	public void testBeanFileLogger() {
		//create the service
		BeanFileLogger logger = new BeanFileLogger();
		// set parameter
		logger.setFileName("tmp/myapp.log");
		// start
		logger.start();
		// use
		use(logger);
		// stop
		logger.stop();
	}
	
	@Order(4)
	@Test
	public void testCalculatorAndStderrLogger() {
//		//create and start the logger service (on stderr)
//		StderrLogger logger = new StderrLogger();
//		logger.start();
		
		// create file logger
		BeanFileLogger logger = new BeanFileLogger();
		logger.setFileName("tmp/myapp.log");
		logger.start();
		// create, inject and start the calculator service
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setLogger(logger);
		calculator.start();
		// use the calculator service
		use(calculator);
		// stop the calculator service
		calculator.stop();
		// stop the logger service
		logger.stop();
	}
}
