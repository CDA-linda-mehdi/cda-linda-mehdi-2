package com.afpa.main;

import java.util.Scanner;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.afpa.api.ITools;


public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ITools tools;
		boolean run = true;
		String str;

		while (run) {
			
			// create a context and find beans
			String conf = "config.xml";
			try (AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(conf);) {
				tools = ctx.getBean(ITools.class);
			}
			
			System.out.println("Veuillez saisir une cha�ne de caract�res : ");
			str = sc.next();
			System.out.println(tools.action(str));
		}
	}

}
