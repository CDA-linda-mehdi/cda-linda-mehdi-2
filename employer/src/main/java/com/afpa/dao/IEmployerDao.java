package com.afpa.dao;

import com.afpa.entity.Employer;

public interface IEmployerDao extends IDao<Employer> {

}
