package com.afpa.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.afpa.dao.IDao;

public class AbstractDao<T> implements IDao<T> {

	private EntityManagerFactory factory = null;
	
	private final Class<T> clazz;
	
	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.clazz = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}

	public void close() {
		if (this.factory != null) {
			this.factory.close();
		}
	}

	public EntityManager newEntityManager() {
		EntityManager em = this.factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}

	public void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
						e.printStackTrace();
					}
				}
				em.close();
			}
		}
	}
	
	
	public T add(T entity) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			em.persist(entity);
			em.getTransaction().commit();
		} finally {
			closeEntityManager(em);
		}
		return entity;
	}

	public List<T> fingAllByNamedQuery(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<T> q = em.createNamedQuery(query, this.clazz);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}
	
}
