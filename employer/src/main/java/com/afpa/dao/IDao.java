package com.afpa.dao;

import java.util.List;

public interface IDao<T> {

	public T add(T entity);
	public List<T> fingAllByNamedQuery(String query);
}
