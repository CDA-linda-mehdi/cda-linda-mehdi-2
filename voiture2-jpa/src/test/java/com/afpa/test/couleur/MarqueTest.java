package com.afpa.test.couleur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.service.IMarqueService;
import com.afpa.service.impl.MarqueServiceImpl;

@TestMethodOrder(OrderAnnotation.class)
public class MarqueTest {
	
	static IMarqueService marqueService;
	
	@BeforeAll
	public static void init() {
		marqueService = new MarqueServiceImpl();
	}
	
	static int idMarqueCree;
	
	@Test
	@Order(1)
	void creation() {
		ReponseDto reponse = marqueService.creerMarque("marque"+System.currentTimeMillis());
		assertNotNull(reponse);
		assertEquals(reponse.getCode(), Status.OK);
		assertNotNull(reponse.getContenu());
		idMarqueCree =((CreationReponseDto) reponse.getContenu()).getCode();
	}
	
}
