package com.afpa.test.couleur;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.afpa.dto.reponse.CreationReponseDto;
import com.afpa.dto.reponse.ElementSimpleDto;
import com.afpa.dto.reponse.ReponseDto;
import com.afpa.dto.reponse.Status;
import com.afpa.dto.requete.ModeleCreationDto;
import com.afpa.service.IEnergieService;
import com.afpa.service.IMarqueService;
import com.afpa.service.IModeleService;
import com.afpa.service.impl.EnergieServiceImpl;
import com.afpa.service.impl.MarqueServiceImpl;
import com.afpa.service.impl.ModeleServiceImpl;

@TestMethodOrder(OrderAnnotation.class)
public class ModeleTest {

	static IModeleService modeleService;
	static IMarqueService marqueService;
	static IEnergieService energieService;

	static String MARQUE_BMW_LABEL = "bmw";
	static int MARQUE_BMW_CODE;

	static String ENERGIE_DIESEL_LABEL = "diesel";
	static int ENERGIE_DIESEL_CODE;

	@BeforeAll
	public static void init() {
		modeleService = new ModeleServiceImpl();
		marqueService = new MarqueServiceImpl();
		energieService = new EnergieServiceImpl();
		{
			ReponseDto reponseMarque = marqueService.chercherMarqueParLabel(MARQUE_BMW_LABEL);
			if (reponseMarque.getCode() != Status.OK) {
				reponseMarque = marqueService.creerMarque(MARQUE_BMW_LABEL);
			}
			assertEquals(reponseMarque.getCode(), Status.OK);
			assertNotNull(reponseMarque.getContenu());
			if (reponseMarque.getContenu() instanceof CreationReponseDto) {
				MARQUE_BMW_CODE = ((CreationReponseDto) reponseMarque.getContenu()).getCode();
			} else {
				MARQUE_BMW_CODE = ((ElementSimpleDto) reponseMarque.getContenu()).getCode();
			}
		}
		{
			ReponseDto reponseEnergie = energieService.chercherEnergieParLabel(ENERGIE_DIESEL_LABEL);
			if (reponseEnergie.getCode() != Status.OK) {
				reponseEnergie = energieService.creerEnergie(ENERGIE_DIESEL_LABEL);
			}
			assertEquals(reponseEnergie.getCode(), Status.OK);
			assertNotNull(reponseEnergie.getContenu());
			if (reponseEnergie.getContenu() instanceof CreationReponseDto) {
				ENERGIE_DIESEL_CODE = ((CreationReponseDto) reponseEnergie.getContenu()).getCode();
			} else {
				ENERGIE_DIESEL_CODE = ((ElementSimpleDto) reponseEnergie.getContenu()).getCode();
			}
		}

	}

	static int codeModele;
	
	@Test
	@Order(1)
	void creation_marque_energie_existe() {
		ReponseDto reponse = modeleService.creerModele(
				ModeleCreationDto.builder()
				.puissance(300)
				.energie(ENERGIE_DIESEL_LABEL)
				.marque(MARQUE_BMW_LABEL)
				.modele("M"+System.currentTimeMillis())
				.build());
		assertNotNull(reponse);
		assertEquals(reponse.getCode(), Status.OK);
		assertNotNull(reponse.getContenu());
		codeModele = ((CreationReponseDto) reponse.getContenu()).getCode();
	}

}
