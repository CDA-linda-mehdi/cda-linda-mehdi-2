package com.afpa.dto.reponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Builder
@ToString
public class ReponseDto {
	private Status code;
	private String msg;
	private IContenuDto contenu;
}
