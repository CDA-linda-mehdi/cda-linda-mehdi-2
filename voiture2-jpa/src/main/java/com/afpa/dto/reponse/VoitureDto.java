package com.afpa.dto.reponse;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class VoitureDto implements IContenuDto {
	private int id;
	private String matricule;
	private int puissance;
	private String energie;
	private String modele;
	private String marque;
	private String couleur;
}
