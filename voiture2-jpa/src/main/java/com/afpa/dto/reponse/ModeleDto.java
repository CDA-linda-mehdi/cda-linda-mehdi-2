package com.afpa.dto.reponse;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class ModeleDto implements IContenuDto {
	private int code;
	private String label;
	private String marque;
}
