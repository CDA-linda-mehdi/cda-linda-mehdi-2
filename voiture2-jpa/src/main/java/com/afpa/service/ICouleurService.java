package com.afpa.service;

import com.afpa.dto.reponse.ReponseDto;

public interface ICouleurService {

	ReponseDto creerCouleur(String marque);

	ReponseDto chercherCouleurParCode(int codeCouleurCree);

	ReponseDto chercherCouleurParLabel(String couleur);

	ReponseDto recupererToutesLesCouleurs();

	ReponseDto mettreAjour(String couleur, String nouveauLabel);

	ReponseDto supprimerCouleur(String couleur);

}
