package com.afpa.ihm;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MenuAction implements Comparable<MenuAction>,Runnable{
	
	private final int id;
	private final String desc;
	private final Runnable runnable;
	
	public final int compareTo(MenuAction o) {
		return Integer.compare(this.id, o.id);
	}

	@Override
	public void run() {
		this.runnable.run();
	}
	
}
