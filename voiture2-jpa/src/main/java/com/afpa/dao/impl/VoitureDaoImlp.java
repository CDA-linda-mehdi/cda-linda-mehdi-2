package com.afpa.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.afpa.dao.IVoitureDao;
import com.afpa.entity.Voiture;

public class VoitureDaoImlp extends AbstractDao<Voiture> implements IVoitureDao {

	@Override
	public Voiture getVoitureByMatricule(String matricule) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Voiture> q = em.createNamedQuery("getVoitureByMatricule", Voiture.class);
			q.setParameter("labelParam", matricule);
			return q.getSingleResult();
		}catch(NoResultException e) {
			return null;
		} finally {
			closeEntityManager(em);
		}
	}

	@Override
	public List<Voiture> findVoituresByColorLabel(String couleur) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Voiture> q = em.createNamedQuery("Voiture.findAll.byColorLabel", Voiture.class);
			q.setParameter("labelParam", couleur);
			return q.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

//	@Override
//	public List<Voiture> findVoituresByEnergieLabel(String energie) {
//		EntityManager em = null;
//		try {
//			em = newEntityManager();
//			TypedQuery<Voiture> q = em.createNamedQuery("Voiture.findAll.byEnergyLabel", Voiture.class);
//			q.setParameter("labelParam", energie);
//			return q.getResultList();
//		} finally {
//			closeEntityManager(em);
//		}
//	}

}
