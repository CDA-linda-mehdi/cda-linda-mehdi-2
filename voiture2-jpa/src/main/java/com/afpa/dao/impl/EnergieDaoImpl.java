package com.afpa.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.afpa.dao.IEnergieDao;
import com.afpa.entity.Energie;

public class EnergieDaoImpl extends AbstractDao<Energie> implements IEnergieDao {

	@Override
	public Energie getEnergieByLabel(String energie) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Energie> q = em.createNamedQuery("getEnergieByLabel", Energie.class);
			q.setParameter("labelParam", energie);
			return q.getSingleResult();
		}catch(NoResultException e) {
			return null;
		} finally {
			closeEntityManager(em);
		}
	}

}
