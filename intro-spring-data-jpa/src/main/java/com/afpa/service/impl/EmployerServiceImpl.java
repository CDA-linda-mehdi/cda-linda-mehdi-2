package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEmployerDao;
import com.afpa.dto.EmployerDto;
import com.afpa.entity.Employer;
import com.afpa.service.IEmployerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployerServiceImpl implements IEmployerService {
	
	@Autowired
	IEmployerDao employerDao;
	
	@Override
	public EmployerDto creer(String nom) {
		List<Employer> employers = this.employerDao.findByNomOrderByNomReouane(nom);
		if(employers != null && employers.size() != 0) {
			log.error("des employers ont deja ce nom : " + employers);
			return null;
		}
		Employer employer = Employer.builder().nom(nom).build();
		
		employer = this.employerDao.save(employer);
		
		return EmployerDto.builder().id(employer.getId()).build();
	}

	@Override
	public List<EmployerDto> lister() {
		Iterator<Employer> iterator = this.employerDao.findAllByOrderByNomDesc().iterator();
		List<EmployerDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Employer x = iterator.next();
			lst.add(EmployerDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public EmployerDto employerParNom(String n) {
		Iterator<Employer> iterator = this.employerDao.findByNomOrderByNomReouane(n).iterator();
		EmployerDto res = null;
		if(iterator.hasNext()) {
			Employer x = iterator.next();
			res = EmployerDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Employer> iterator = this.employerDao.findByNomOrderByNomReouane(oldName).iterator();
		EmployerDto res = null;
		if(iterator.hasNext()) {
			Employer x = iterator.next();
			x.setNom(newName);
			this.employerDao.save(x);
		}
	}

}
