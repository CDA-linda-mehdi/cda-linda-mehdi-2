package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Employer;

@Repository
public interface IEmployerDao extends CrudRepository<Employer,Integer> {

	List<Employer> findByNomOrderByNomReouane(@Param("nomParam")String p);

//	@Query("select e from Employer e order by e.nom asc")
	List<Employer> findAllByOrderByNomDesc();

}
