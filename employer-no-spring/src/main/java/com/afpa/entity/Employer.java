package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_employer")
@Entity
@NamedQueries({
	@NamedQuery(name = "Employer.parNom",query = "select e from Employer e where e.nom= :nomParam"),
	@NamedQuery(name = "Employer.findAll",query = "select e from Employer e"),
	@NamedQuery(name = "Employer.findAllByName",query = "select e from Employer e order by e.nom")
})
public class Employer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String nom;
	
	private int salaire;
}
