package com.afpa.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEmployerDao;
import com.afpa.dto.EmployerDto;
import com.afpa.entity.Employer;
import com.afpa.service.IEmployerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployerServiceImpl implements IEmployerService {
	
	@Autowired
	IEmployerDao employerDao;
	
//	public EmployerServiceImpl() {
//		employerDao = new EmployerDaoImpl();
//	}

	@Override
	public EmployerDto creer(String nom) {
		List<Employer> employers = this.employerDao.trouverParNom(nom);
		if(employers != null && employers.size() != 0) {
			log.error("des employers ont deja ce nom : " + employers);
			return null;
		}
		Employer employer = Employer.builder().nom(nom).build();
		
		employer = this.employerDao.add(employer);
		
		return EmployerDto.builder().id(employer.getId()).build();
	}

	@Override
	public List<EmployerDto> lister() {
		return this.employerDao.findAllByNamedQuery("Employer.findAllByName")
				.stream()
				.map(x->EmployerDto.builder()
						.id(x.getId())
						.nom(x.getNom())
						.build())
				.collect(Collectors.toList());
	}

}
