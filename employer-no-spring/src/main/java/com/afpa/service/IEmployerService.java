package com.afpa.service;

import java.util.List;

import com.afpa.dto.EmployerDto;

public interface IEmployerService {

	EmployerDto creer(String nom);

	List<EmployerDto> lister();

}
