package com.afpa.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.afpa.dto.EmployerDto;
import com.afpa.service.IEmployerService;
import com.afpa.service.impl.EmployerServiceImpl;

public class EmployerTest {
	
	static IEmployerService employerService;
	
	@BeforeAll
	static void init() {
		employerService = new EmployerServiceImpl();
	}
	
	@Test
	void creation(){
		String nom = "Yassine";
		EmployerDto emp = employerService.creer(nom);
		assertNotNull(emp);
		assertNotNull(emp.getId());
	}
}
