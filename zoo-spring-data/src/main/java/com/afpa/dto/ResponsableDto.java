package com.afpa.dto;

import com.afpa.entite.Enclos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter
@Setter
public class ResponsableDto {
	private int id;
	private String nom;
	private Enclos enclos;
}
