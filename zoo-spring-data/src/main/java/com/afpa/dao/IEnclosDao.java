package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entite.Enclos;

@Repository
public interface IEnclosDao extends CrudRepository<Enclos, Integer> {

	List<Enclos> findByNom(@Param("nomParam")String n);
	
	List<Enclos> findAllByOrderByNomAsc();
}
