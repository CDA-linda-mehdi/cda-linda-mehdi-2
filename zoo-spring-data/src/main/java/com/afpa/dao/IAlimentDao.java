package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entite.Aliment;

@Repository
public interface IAlimentDao extends CrudRepository<Aliment, Integer> {
	
	
	List<Aliment> findByNom(@Param("nomParam")String n);
	
	List<Aliment> findAllByOrderByNomAsc();
}
