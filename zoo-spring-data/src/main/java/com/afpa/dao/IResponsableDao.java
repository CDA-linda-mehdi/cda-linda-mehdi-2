package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entite.Aliment;
import com.afpa.entite.Responsable;

@Repository
public interface IResponsableDao extends CrudRepository<Responsable, Integer> {

	List<Responsable> findByNom(@Param("nomParam")String n);
	
	List<Responsable> findAllByOrderByNomAsc();
}
