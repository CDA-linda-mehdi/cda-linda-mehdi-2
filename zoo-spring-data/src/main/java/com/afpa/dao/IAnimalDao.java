package com.afpa.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entite.Animal;
import com.afpa.entite.Animal;

@Repository
public interface IAnimalDao extends CrudRepository<Animal, Integer >{


	List<Animal> findByNom(@Param("nomParam")String n);
	
	List<Animal> findAllByOrderByNomAsc();
}
