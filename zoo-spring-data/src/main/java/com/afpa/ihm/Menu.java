package com.afpa.ihm;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.afpa.dto.AlimentDto;
import com.afpa.dto.AnimalDto;
import com.afpa.dto.EnclosDto;
import com.afpa.dto.ResponsableDto;
import com.afpa.service.IAlimentService;
import com.afpa.service.IAnimalService;
import com.afpa.service.IEnclosService;
import com.afpa.service.IResponsableService;

public class Menu {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();

		IAlimentService alimentService = ctx.getBean(IAlimentService.class);
		IAnimalService animalService = ctx.getBean(IAnimalService.class);
		IResponsableService responsableService = ctx.getBean(IResponsableService.class);
		IEnclosService enclosService = ctx.getBean(IEnclosService.class);

		boolean bool = true;
		int choix = -1;

		while (bool) {
			System.out.println("**********************************");
			System.out.println("********* Menu principal *********");
			System.out.println("**********************************");
			
			System.out.println("0 - Arrêter le menu");

			System.out.println("1 - Ajouter un aliment");
			System.out.println("2 - Afficher la liste des aliments");
			System.out.println("3 - Trouver un aliment pas son nom");
			System.out.println("4 - Mettre à jour un aliment");
			System.out.println("5 - Supprimer un aliment");

			System.out.println("6 - Ajouter un animal");
			System.out.println("7 - Afficher la liste des animaux");
			System.out.println("8 - Trouver un animal pas son nom");
			System.out.println("9 - Mettre à jour un animal");
			System.out.println("10 - Supprimer un animal");

			System.out.println("11 - Ajouter un responsable");
			System.out.println("12 - Afficher la liste des responsables");
			System.out.println("13 - Trouver un responsable pas son nom");
			System.out.println("14 - Mettre à jour un responsable");
			System.out.println("15 - Supprimer un responsable");
			
			System.out.println("16 - Ajouter un enclos");
			System.out.println("17 - Afficher la liste des enclos");
			System.out.println("18 - Trouver un enclos pas son nom");
			System.out.println("19 - Mettre à jour un enclos");
			System.out.println("20 - Supprimer un enclos");

			choix = sc.nextInt();
			sc.nextLine();

			switch (choix) {
			case 0:
				System.out.println("Menu fermé !");
				bool = false;
				break;
			case 1:
				System.out.println("saisir le nom de l'aliment : ");
				System.out.println("> ");
				String nom = sc.nextLine();
				AlimentDto creer = alimentService.creerAliment(nom);
				if (creer == null) {
					System.out.println("erreur de création");
				} else {
					System.out.println("l'aliment " + creer.getId() + " a été créé");
				}
				break;
			case 2:
				List<AlimentDto> lst = alimentService.listerAliments();
				System.out.println("la liste des aliment : ");
				lst.forEach(System.out::println);
				break;
			case 3:
				System.out.println("saisir le nom de l'aliment : ");
				String n = sc.nextLine();
				AlimentDto ali = alimentService.trouverAlimentParNom(n);
				System.out.println("l'aliment : " + ali);
				break;
			case 4:
				System.out.println("saisir le nom actuel de l'aliment : ");
				String oldName = sc.nextLine();

				System.out.println("saisir le nouveau nom de l'aliment : ");
				String newName = sc.nextLine();

				alimentService.miseAJourNom(oldName, newName);
				break;
			case 5:
				System.out.println("saisir le nom de l'aliment à supprimer ; ");
				String no = sc.nextLine();
				alimentService.supprimerAlimentParNom(no);
				break;
			case 6:
				System.out.println("saisir le nom de l'animal : ");
				System.out.println("> ");
				String nomAnimal = sc.nextLine();
				AnimalDto creerAnimal = animalService.creerAnimal(nomAnimal);
				if (creerAnimal == null) {
					System.out.println("erreur de création");
				} else {
					System.out.println("l'animal" + creerAnimal.getId() + " a été créé");
				}
				break;
			case 7:
				List<AnimalDto> lstAnimaux = animalService.listerAnimaux();
				System.out.println("la liste des animaux : ");
				lstAnimaux.forEach(System.out::println);
				break;
			case 8:
				System.out.println("saisir le nom de l'animal : ");
				String nAnimal = sc.nextLine();
				AnimalDto ani = animalService.trouverAnimalParNom(nAnimal);
				System.out.println("l'animal: " + ani);
				break;
			case 9:
				System.out.println("saisir le nom actuel de l'animal : ");
				String oldNameAnimal = sc.nextLine();

				System.out.println("saisir le nouveau nom de l'animal : ");
				String newNameAnimal = sc.nextLine();

				animalService.miseAJourNom(oldNameAnimal, newNameAnimal);
				break;
			case 10:
				System.out.println("saisir le nom de l'animal à supprimer ; ");
				String noAnimal = sc.nextLine();
				animalService.supprimerAnimalParNom(noAnimal);
				break;
			case 11:
				System.out.println("saisir le nom du responsable : ");
				System.out.println("> ");
				String nomResponsable = sc.nextLine();
				System.out.println("saisir le nom de l'enclos : ");
				System.out.println("> ");
				String nomEnclosDuRes = sc.nextLine();
				ResponsableDto creerResponsable = responsableService.creerResponsable(nomResponsable, nomEnclosDuRes);
				if (creerResponsable == null) {
					System.out.println("erreur de création");
				} else {
					System.out.println("le responsable " + creerResponsable.getId() + " a été créé");
				}
				//EnclosDto ajoutEnclos = responsableService.ajouterEnclos(nomEnclosDuRes);
				break;
			case 12:
				List<ResponsableDto> listResponsable = responsableService.listerResponsable();
				System.out.println("la liste des responsables : ");
				listResponsable.forEach(System.out::println);
				break;
			case 13:
				System.out.println("saisir le nom du responsable : ");
				String nResponsable = sc.nextLine();
				ResponsableDto resp = responsableService.trouverResponsableParNom(nResponsable);
				System.out.println("l'aliment : " + resp);
				break;
			case 14:
				System.out.println("saisir le nom actuel du responsable : ");
				String oldNameResponsable = sc.nextLine();

				System.out.println("saisir le nouveau nom du responsable : ");
				String newNameResponsable = sc.nextLine();

				responsableService.miseAJourNomResponsable(oldNameResponsable, newNameResponsable);
				break;
			case 15:
				System.out.println("saisir le nom du responsable à supprimer ; ");
				String noResponsable = sc.nextLine();
				responsableService.supprimerResponsableParNom(noResponsable);
				break;
			case 16:
				System.out.println("saisir le nom de l'enclos : ");
				System.out.println("> ");
				String nomEnclos = sc.nextLine();
				EnclosDto creerEnclos = enclosService.creerEnclos(nomEnclos);
				if (creerEnclos == null) {
					System.out.println("erreur de création");
				} else {
					System.out.println("l'enclos" + creerEnclos.getId() + " a été créé");
				}
				break;
			case 17:
				List<EnclosDto> lstEnclos = enclosService.listerEnclos();
				System.out.println("la liste des enclos : ");
				lstEnclos.forEach(System.out::println);
				break;
			case 18:
				System.out.println("saisir le nom de l'enclos : ");
				String nEnclos = sc.nextLine();
				EnclosDto enc = enclosService.trouverEnclosParNom(nEnclos);
				System.out.println("l'enclos: " + enc);
				break;
			case 19:
				System.out.println("saisir le nom actuel de l'enclos : ");
				String oldNameEncos = sc.nextLine();

				System.out.println("saisir le nouveau nom de l'enclo : ");
				String newNameEnclos = sc.nextLine();

				enclosService.miseAJourNom(oldNameEncos, newNameEnclos);
				break;
			case 20:
				System.out.println("saisir le nom de l'enclos à supprimer ; ");
				String noEnclos = sc.nextLine();
				enclosService.supprimerEnclosParNom(noEnclos);
				break;
			default:
				break;
			}

			System.out.println("\n==========================");
		}

		sc.close();
	}

}
