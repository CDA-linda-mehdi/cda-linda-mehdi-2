package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEnclosDao;
import com.afpa.dto.AlimentDto;
import com.afpa.dto.EnclosDto;
import com.afpa.entite.Aliment;
import com.afpa.entite.Enclos;
import com.afpa.service.IEnclosService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EnclosServiceImpl implements IEnclosService {

	@Autowired
	IEnclosDao enclosDao;
	
	@Override
	public EnclosDto creerEnclos(String nom) {
		List<Enclos> listEnclos = this.enclosDao.findByNom(nom);
		if(listEnclos != null && listEnclos.size() != 0) {
			log.error("des enclos ont déjà ce nom : " +listEnclos);
			return null;
		}
		Enclos enclos = Enclos.builder().nom(nom).build();
		
		enclos = this.enclosDao.save(enclos);
		
		return EnclosDto.builder().id(enclos.getId()).build();
	}

	@Override
	public List<EnclosDto> listerEnclos() {
		Iterator<Enclos> iterator = this.enclosDao.findAllByOrderByNomAsc().iterator();
		List<EnclosDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Enclos x = iterator.next();
			lst.add(EnclosDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public EnclosDto trouverEnclosParNom(String nom) {
		Iterator<Enclos> iterator = this.enclosDao.findByNom(nom).iterator();
		EnclosDto res = null;
		if(iterator.hasNext()) {
			Enclos x = iterator.next();
			res = EnclosDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAJourNom(String oldNom, String newNom) {
		Iterator<Enclos> iterator = this.enclosDao.findByNom(oldNom).iterator();
		EnclosDto res = null;
		if(iterator.hasNext()) {
			Enclos x = iterator.next();
			x.setNom(newNom);
			this.enclosDao.save(x);
		}
		
	}

	@Override
	public void supprimerEnclosParNom(String nom) {
		Iterator<Enclos> iterator = this.enclosDao.findByNom(nom).iterator();
		EnclosDto res = null;
		if(iterator.hasNext()) {
			Enclos x = iterator.next();
			this.enclosDao.delete(x);
		}
		
	}

}
