package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IEnclosDao;
import com.afpa.dao.IResponsableDao;
import com.afpa.dto.EnclosDto;
import com.afpa.dto.ResponsableDto;
import com.afpa.entite.Enclos;
import com.afpa.entite.Responsable;
import com.afpa.service.IResponsableService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ResponsableServiceImpl implements IResponsableService {

	@Autowired
	IResponsableDao responsableDao;
	
	@Autowired
	IEnclosDao enclosDao;
	
	@Override
	public ResponsableDto creerResponsable(String nom, String nomEnclos) {
		List<Responsable> responsables = this.responsableDao.findByNom(nom);
		if(responsables != null && responsables.size() != 0) {
			log.error("des responsables ont déjà ce nom : " +responsables);
			return null;
		}
		Responsable responsable = Responsable.builder().nom(nom).enclos(ajouterEnclos(nomEnclos)).build();
		
		responsable = this.responsableDao.save(responsable);
		
		return ResponsableDto.builder().id(responsable.getId()).build();
	}

	@Override
	public List<ResponsableDto> listerResponsable() {
		Iterator<Responsable> iterator = this.responsableDao.findAllByOrderByNomAsc().iterator();
		List<ResponsableDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Responsable x = iterator.next();
			lst.add(ResponsableDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public ResponsableDto trouverResponsableParNom(String nom) {
		Iterator<Responsable> iterator = this.responsableDao.findByNom(nom).iterator();
		ResponsableDto res = null;
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			res = ResponsableDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAJourNomResponsable(String oldNom, String newNom) {
		Iterator<Responsable> iterator = this.responsableDao.findByNom(oldNom).iterator();
		ResponsableDto res = null;
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			x.setNom(newNom);
			this.responsableDao.save(x);
		}
		
	}

	@Override
	public void supprimerResponsableParNom(String nom) {
		Iterator<Responsable> iterator = this.responsableDao.findByNom(nom).iterator();
		if(iterator.hasNext()) {
			Responsable x = iterator.next();
			this.responsableDao.delete(x);
		}
		
	}

	@Override
	public Enclos ajouterEnclos(String nomEnclos) {
		Iterator<Enclos> iterator = this.enclosDao.findByNom(nomEnclos).iterator();
		Enclos res = null;
		if(iterator.hasNext()) {
			Enclos x = iterator.next();
			res = Enclos.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build();
		}
		return res;
	}

}
