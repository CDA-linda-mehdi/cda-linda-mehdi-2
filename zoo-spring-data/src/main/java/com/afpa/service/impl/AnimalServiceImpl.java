package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IAnimalDao;
import com.afpa.dto.AnimalDto;
import com.afpa.entite.Animal;
import com.afpa.service.IAnimalService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AnimalServiceImpl implements IAnimalService {

	@Autowired
	IAnimalDao animalDao;
	
	@Override
	public AnimalDto creerAnimal(String nom) {
		List<Animal> Animals = this.animalDao.findByNom(nom);
		if(Animals != null && Animals.size() != 0) {
			log.error("des Animals ont déjà ce nom : " +Animals);
			return null;
		}
		Animal animal = Animal.builder().nom(nom).build();
		
		animal = this.animalDao.save(animal);
		
		return AnimalDto.builder().id(animal.getId()).build();
	}

	@Override
	public List<AnimalDto> listerAnimaux() {
		Iterator<Animal> iterator = this.animalDao.findAllByOrderByNomAsc().iterator();
		List<AnimalDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Animal x = iterator.next();
			lst.add(AnimalDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public AnimalDto trouverAnimalParNom(String nom) {
		Iterator<Animal> iterator = this.animalDao.findByNom(nom).iterator();
		AnimalDto res = null;
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			res = AnimalDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAJourNom(String noldNom, String newNom) {
		Iterator<Animal> iterator = this.animalDao.findByNom(noldNom).iterator();
		AnimalDto res = null;
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			x.setNom(newNom);
			this.animalDao.save(x);
		}
		
	}

	@Override
	public void supprimerAnimalParNom(String nom) {
		Iterator<Animal> iterator = this.animalDao.findByNom(nom).iterator();
		AnimalDto res = null;
		if(iterator.hasNext()) {
			Animal x = iterator.next();
			this.animalDao.delete(x);
		}
		
	}

}
