package com.afpa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.IAlimentDao;
import com.afpa.dto.AlimentDto;
import com.afpa.entite.Aliment;
import com.afpa.service.IAlimentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlimentServiceImpl implements IAlimentService {

	@Autowired
	IAlimentDao alimentDao;
	
	@Override
	public AlimentDto creerAliment(String nom) {
		List<Aliment> aliments = this.alimentDao.findByNom(nom);
		if(aliments != null && aliments.size() != 0) {
			log.error("des aliments ont déjà ce nom : " +aliments);
			return null;
		}
		Aliment aliment = Aliment.builder().nom(nom).build();
		
		aliment = this.alimentDao.save(aliment);
		
		return AlimentDto.builder().id(aliment.getId()).build();
	}

	@Override
	public List<AlimentDto> listerAliments() {
		Iterator<Aliment> iterator = this.alimentDao.findAllByOrderByNomAsc().iterator();
		List<AlimentDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Aliment x = iterator.next();
			lst.add(AlimentDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public AlimentDto trouverAlimentParNom(String nom) {
		Iterator<Aliment> iterator = this.alimentDao.findByNom(nom).iterator();
		AlimentDto res = null;
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			res = AlimentDto.builder()
					.id(x.getId())
					.nom(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAJourNom(String oldNom, String newNom) {
		Iterator<Aliment> iterator = this.alimentDao.findByNom(oldNom).iterator();
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			x.setNom(newNom);
			this.alimentDao.save(x);
		}
		
	}

	@Override
	public void supprimerAlimentParNom(String nom) {
		Iterator<Aliment> iterator = this.alimentDao.findByNom(nom).iterator();
		AlimentDto res = null;
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			this.alimentDao.delete(x);
		}
		
	}

}
