package com.afpa.service;

import java.util.List;

import com.afpa.dto.EnclosDto;

public interface IEnclosService {

	EnclosDto creerEnclos(String nom);
	
	List<EnclosDto> listerEnclos();
	
	EnclosDto trouverEnclosParNom(String nom);
	
	void miseAJourNom(String oldNom, String newNom);
	
	void supprimerEnclosParNom(String nom);
}
