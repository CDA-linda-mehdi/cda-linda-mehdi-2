package com.afpa.service;

import java.util.List;

import com.afpa.dto.EnclosDto;
import com.afpa.dto.ResponsableDto;
import com.afpa.entite.Enclos;

public interface IResponsableService {

	ResponsableDto creerResponsable(String nom, String nomEnclos);

	List<ResponsableDto> listerResponsable();

	ResponsableDto trouverResponsableParNom(String nom);

	void miseAJourNomResponsable(String oldNom, String newNom);

	void supprimerResponsableParNom(String nom);
	
	Enclos ajouterEnclos(String nomEnclos);
}
