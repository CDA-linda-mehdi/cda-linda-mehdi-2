package com.afpa.service;

import java.util.List;

import com.afpa.dto.AlimentDto;

public interface IAlimentService{

	AlimentDto creerAliment(String nom);
	
	List<AlimentDto> listerAliments();
	
	AlimentDto trouverAlimentParNom(String nom);
	
	void miseAJourNom(String oldNom, String newNom);
	
	void supprimerAlimentParNom(String nom);
	
}
