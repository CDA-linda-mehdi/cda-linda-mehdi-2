package com.afpa.service;

import java.util.List;

import com.afpa.dto.AnimalDto;
import com.afpa.dto.AnimalDto;
import com.afpa.entite.Animal;

public interface IAnimalService {

	AnimalDto creerAnimal(String nom);

	List<AnimalDto> listerAnimaux();

	AnimalDto trouverAnimalParNom(String nom);

	void miseAJourNom(String oldNom, String newNom);

	void supprimerAnimalParNom(String nom);
}
