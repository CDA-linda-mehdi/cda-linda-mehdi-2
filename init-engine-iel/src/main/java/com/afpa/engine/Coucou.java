package com.afpa.engine;

import org.springframework.stereotype.Service;

import com.afpa.api.ITools;

@Service
public class Coucou implements ITools {

	public String action(String s) {
		return new StringBuilder(s).reverse().toString();
	}

}
